package com.bukanavatar.pagingpokemon.data

import android.arch.lifecycle.MutableLiveData
import android.arch.paging.LivePagedListBuilder
import com.bukanavatar.pagingpokemon.api.PokemonService
import com.bukanavatar.pagingpokemon.db.PokemonLocalCache
import com.bukanavatar.pagingpokemon.model.PokeSearchResult

class Pokemon(
        private val service: PokemonService,
        private val cache: PokemonLocalCache
) {
    private val lastPage = 1
    private val networkError = MutableLiveData<String>()

    private var isRequestProgress = false

    companion object {
        private const val DATABASE_PAGE_SIZE = 5
    }

    fun search(query: String): PokeSearchResult {
        val dataSourceFactory = cache.pokeByName(query)
        val boundaryCallback = BoundaryCallback(query, service, cache)
        val networkError = boundaryCallback.networkError

        val data = LivePagedListBuilder(dataSourceFactory, DATABASE_PAGE_SIZE)
                .setBoundaryCallback(boundaryCallback)
                .build()

        return PokeSearchResult(data, networkError)
    }
}