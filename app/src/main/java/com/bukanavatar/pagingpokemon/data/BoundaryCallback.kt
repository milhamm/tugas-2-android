package com.bukanavatar.pagingpokemon.data

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.paging.PagedList
import android.util.Log
import com.bukanavatar.pagingpokemon.api.PokemonService
import com.bukanavatar.pagingpokemon.api.searchPokemon
import com.bukanavatar.pagingpokemon.db.PokemonLocalCache
import com.bukanavatar.pagingpokemon.model.PokemonModel

class BoundaryCallback(
        private val query: String,
        private val service: PokemonService,
        private val cache: PokemonLocalCache
) : PagedList.BoundaryCallback<PokemonModel>() {
    companion object {
        private const val NETWORKPAGESIZE = 20
    }

    private var isRequestInProgress = false

    private var lastRequestedPage = 1
    private val _networkErrors = MutableLiveData<String>()

    val networkError: LiveData<String>
        get() = _networkErrors

    private fun requestAndSaveData(query: String) {
        if (isRequestInProgress) return

        isRequestInProgress = true
        searchPokemon(service, lastRequestedPage, NETWORKPAGESIZE, { pokemons ->
            cache.insert(pokemons, {

            })
            Log.d("paging", "load page" + lastRequestedPage)
            lastRequestedPage++
            isRequestInProgress = false
        }, { error ->
            _networkErrors.postValue(error)
            isRequestInProgress = false
        })
    }

    override fun onZeroItemsLoaded() {
        requestAndSaveData(query)
    }

    override fun onItemAtEndLoaded(itemAtEnd: PokemonModel) {
        requestAndSaveData(query)
    }
}