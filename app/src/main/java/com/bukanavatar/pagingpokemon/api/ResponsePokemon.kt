package com.bukanavatar.pagingpokemon.api

import com.bukanavatar.pagingpokemon.model.PokemonModel

data class ResponsePokemon(
        val Pokemon: List<PokemonModel> = emptyList()
)