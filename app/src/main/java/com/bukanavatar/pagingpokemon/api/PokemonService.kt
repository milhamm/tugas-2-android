package com.bukanavatar.pagingpokemon.api

import android.content.ContentValues.TAG
import android.util.Log
import com.bukanavatar.pagingpokemon.model.PokemonModel
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST

fun searchPokemon(
        service: PokemonService,
        offset: Int,
        limit: Int,
        onSuccess: (pokemon: List<PokemonModel>) -> Unit,
        onError: (error: String) -> Unit) {

    service.searchPokemon((offset - 1) * limit, limit).enqueue(
            object : Callback<ResponsePokemon> {
                override fun onFailure(call: retrofit2.Call<ResponsePokemon>, t: Throwable) {
                    Log.d(TAG, "fail to get data")
                    onError(t.message ?: "unknown error")
                }

                override fun onResponse(call: retrofit2.Call<ResponsePokemon>?, response: Response<ResponsePokemon>) {
                    if (response.isSuccessful) {
                        val pokemon = response.body()?.Pokemon ?: emptyList()
                        onSuccess(pokemon)
                    } else {
                        onError(response.errorBody()?.string() ?: "Unknown Error")
                    }
                }

            }
    )
}


interface PokemonService {
    @POST("/pokemon")
    @FormUrlEncoded
    fun searchPokemon(@Field("offset") offset: Int,
                      @Field("limit") limit: Int): Call<ResponsePokemon>

    companion object {

        fun create(ip: String): PokemonService {
            val logger = HttpLoggingInterceptor()
            logger.level = HttpLoggingInterceptor.Level.BASIC

            val client = OkHttpClient.Builder()
                    .addInterceptor(logger)
                    .build()
            Log.d("AIPI", "ip = $ip")
            return Retrofit.Builder()
                    .baseUrl("http://" + ip + ":5000")
                    .client(client)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build()
                    .create(PokemonService::class.java)
        }
    }
}