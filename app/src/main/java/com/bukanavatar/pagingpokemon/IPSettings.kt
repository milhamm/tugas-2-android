package com.bukanavatar.pagingpokemon

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_ipsettings.*

class IPSettings : AppCompatActivity() {
    lateinit var preferences: SharedPreferences

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_ipsettings)
        preferences = getSharedPreferences(Preferences.PREFERENCES_NAME.ip, Context.MODE_PRIVATE)
        if (preferences.contains(Preferences.IP.ip)) {
            editTextIp.setText(preferences.getString(Preferences.IP.ip, ""))
        }
        buttonSubmitIp.setOnClickListener {
            val edit = preferences.edit()
            edit.putString(Preferences.IP.ip, editTextIp.text.toString())
            edit.apply()
            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
        }
    }
}
