package com.bukanavatar.pagingpokemon

import android.arch.paging.PagedListAdapter
import android.support.v7.util.DiffUtil
import android.support.v7.widget.RecyclerView
import android.view.ViewGroup
import com.bukanavatar.pagingpokemon.model.PokemonModel

class PokemonAdapter : PagedListAdapter<PokemonModel, RecyclerView.ViewHolder>(POKEMON_COMPARATOR) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return PokemonViewHolder.create(parent)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val pokeItem = getItem(position)
        if (pokeItem != null) {
            (holder as PokemonViewHolder).bind(pokeItem)
        }
    }


    companion object {
        private val POKEMON_COMPARATOR = object : DiffUtil.ItemCallback<PokemonModel>() {
            override fun areItemsTheSame(oldItem: PokemonModel, newItem: PokemonModel): Boolean =
                    oldItem.name == newItem.name

            override fun areContentsTheSame(oldItem: PokemonModel, newItem: PokemonModel): Boolean =
                    oldItem == newItem
        }
    }
}