package com.bukanavatar.pagingpokemon

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import com.bukanavatar.pagingpokemon.data.Pokemon

class ViewModelFactory(private val pokemon: Pokemon) : ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(SearchPokemonViewModel::class.java)) {
            @Suppress("UNCHECKED_CAST")
            return SearchPokemonViewModel(pokemon) as T
        }
        throw IllegalArgumentException("Unknown ViewModel Class")
    }

}