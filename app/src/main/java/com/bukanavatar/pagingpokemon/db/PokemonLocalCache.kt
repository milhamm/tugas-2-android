package com.bukanavatar.pagingpokemon.db

import android.arch.paging.DataSource
import com.bukanavatar.pagingpokemon.model.PokemonModel
import java.util.concurrent.Executor

class PokemonLocalCache(
        private val pokeDao: PokemonDao,
        private val ioExecutor: Executor
) {
    fun insert(pokemon: List<PokemonModel>, insertFinsihed: () -> Unit) {
        ioExecutor.execute {
            pokeDao.insert(pokemon)
            insertFinsihed
        }
    }

    fun pokeByName(name: String): DataSource.Factory<Int, PokemonModel> {
        val query = "%${name.replace(' ', '%')}%"
        return pokeDao.pokemonName(query)
    }
}