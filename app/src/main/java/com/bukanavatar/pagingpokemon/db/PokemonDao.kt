package com.bukanavatar.pagingpokemon.db

import android.arch.paging.DataSource
import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy
import android.arch.persistence.room.Query
import com.bukanavatar.pagingpokemon.model.PokemonModel

@Dao
interface PokemonDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(pokemon: List<PokemonModel>)

    @Query("SELECT * FROM pokemon WHERE (name LIKE :queryString)" + "")
    fun pokemonName(queryString: String): DataSource.Factory<Int, PokemonModel>
}