package com.bukanavatar.pagingpokemon

enum class Preferences(val ip: String) {
    PREFERENCES_NAME("pokemon_IP"),
    IP("ip_address")
}