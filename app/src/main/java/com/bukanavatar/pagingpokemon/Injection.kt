package com.bukanavatar.pagingpokemon

import android.arch.lifecycle.ViewModelProvider
import android.content.Context
import com.bukanavatar.pagingpokemon.api.PokemonService
import com.bukanavatar.pagingpokemon.data.Pokemon
import com.bukanavatar.pagingpokemon.db.PokemonDatabase
import com.bukanavatar.pagingpokemon.db.PokemonLocalCache
import java.util.concurrent.Executors

object Injection {
    private fun provideCache(context: Context): PokemonLocalCache {
        val database = PokemonDatabase.getInstance(context)
        return PokemonLocalCache(database.pokemonDao(), Executors.newSingleThreadExecutor())
    }

    private fun providePokemon(context: Context): Pokemon {
        return Pokemon(PokemonService.create(context.getSharedPreferences(Preferences.PREFERENCES_NAME.ip, Context.MODE_PRIVATE).getString(Preferences.IP.ip, "")!!), provideCache(context))
    }

    fun provideViewModelFactory(context: Context): ViewModelProvider.Factory {
        return ViewModelFactory(providePokemon(context))
    }
}
