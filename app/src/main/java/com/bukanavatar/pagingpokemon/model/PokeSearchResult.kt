package com.bukanavatar.pagingpokemon.model

import android.arch.lifecycle.LiveData
import android.arch.paging.PagedList

data class PokeSearchResult(
        val data: LiveData<PagedList<PokemonModel>>,
        val networkError: LiveData<String>
)