package com.bukanavatar.pagingpokemon

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.arch.paging.PagedList
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.DividerItemDecoration
import android.util.Log
import android.view.Menu
import android.view.View
import android.widget.SearchView
import android.widget.Toast
import com.bukanavatar.pagingpokemon.model.PokemonModel
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private lateinit var viewModel: SearchPokemonViewModel
    private val adapter = PokemonAdapter()
    lateinit var preferences: SharedPreferences
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        preferences = getSharedPreferences(Preferences.PREFERENCES_NAME.ip, Context.MODE_PRIVATE)
    }

    override fun onResume() {
        super.onResume()
        if (!preferences.contains(Preferences.IP.ip)) {
            val intent = Intent(this, IPSettings::class.java)
            startActivity(intent)
        } else {
            viewModel = ViewModelProviders.of(this, Injection.provideViewModelFactory(this))
                    .get(SearchPokemonViewModel::class.java)

            val decoration = DividerItemDecoration(this, DividerItemDecoration.VERTICAL)
            list.addItemDecoration(decoration)

            initAdapter()
            viewModel.searchPokemon("")
        }
    }
    private fun initAdapter() {
        list.adapter = adapter
        viewModel.poke.observe(this, Observer<PagedList<PokemonModel>> {
            Log.d("Activity", "list: ${it?.size}")
            showEmptyList(it?.size == 0)
            adapter.submitList(it)
        })
        viewModel.networkError.observe(this, Observer<String> {
            Toast.makeText(this, "\uD83D\uDE28 Wooops ${it}", Toast.LENGTH_LONG).show()
        })
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu, menu)
        val searchItem = menu!!.findItem(R.id.search)
        val searchView = (searchItem.actionView) as SearchView
        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                return false

            }

            override fun onQueryTextChange(newText: String?): Boolean {
                list.scrollToPosition(0)
                viewModel.searchPokemon(newText!!)
                adapter.submitList(null)
                return false
            }
        })
        return super.onCreateOptionsMenu(menu)
    }

    private fun showEmptyList(show: Boolean) {
        if (show) {
            emptyList.visibility = View.VISIBLE
            list.visibility = View.GONE
        } else {
            emptyList.visibility = View.GONE
            list.visibility = View.VISIBLE
        }
    }

    companion object {
        private const val LAST_SEARCH_QUERY: String = "last_search_query"
        private const val DEFAULT_QUERY = " "
    }
}
