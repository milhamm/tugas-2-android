package com.bukanavatar.pagingpokemon

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.bukanavatar.pagingpokemon.model.PokemonModel

class PokemonViewHolder(val view: View) : RecyclerView.ViewHolder(view) {
    private val name: TextView = view.findViewById(R.id.pokemonName)
    private val type: TextView = view.findViewById(R.id.pokemonType)

    private var pokemonModel: PokemonModel? = null

    fun bind(pokemonModel: PokemonModel?) {
        if (pokemonModel == null) {
            val resources = itemView.resources
        } else {
            showPokemonData(pokemonModel)
        }
    }

    private fun showPokemonData(pokemonModel: PokemonModel) {
        this.pokemonModel = pokemonModel
        name.text = pokemonModel.name
        type.text = pokemonModel.type.toString()
    }

    companion object {
        fun create(parent: ViewGroup): PokemonViewHolder {
            val view = LayoutInflater.from(parent.context)
                    .inflate(R.layout.pokemon_item, parent, false)
            return PokemonViewHolder(view)
        }
    }
}