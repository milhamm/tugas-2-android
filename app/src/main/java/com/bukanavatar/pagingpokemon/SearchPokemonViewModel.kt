package com.bukanavatar.pagingpokemon

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.Transformations
import android.arch.lifecycle.ViewModel
import android.arch.paging.PagedList
import com.bukanavatar.pagingpokemon.data.Pokemon
import com.bukanavatar.pagingpokemon.model.PokeSearchResult
import com.bukanavatar.pagingpokemon.model.PokemonModel

class SearchPokemonViewModel(private val pokemon: Pokemon) : ViewModel() {
    companion object {
        private const val VISIBLE_TRESHOLD = 5
    }

    private val queryLiveData = MutableLiveData<String>()
    private val pokeResult: LiveData<PokeSearchResult> = Transformations.map(queryLiveData, {
        pokemon.search(it)
    })

    val poke: LiveData<PagedList<PokemonModel>> = Transformations.switchMap(pokeResult, { it -> it.data })
    val networkError: LiveData<String> = Transformations.switchMap(pokeResult, { it -> it.networkError })

    fun searchPokemon(queryString: String) {
        queryLiveData.postValue(queryString)
    }

    fun lastQueryValue(): String? = queryLiveData.value
}